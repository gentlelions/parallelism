package com.concurrent.thread.dao;

import com.concurrent.thread.entity.Users;
import com.concurrent.thread.util.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author ThinkPad
 */
//@Repository
public interface UsersMapper extends BaseMapper<Users> {
}
