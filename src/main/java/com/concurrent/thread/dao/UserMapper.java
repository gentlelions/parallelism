package com.concurrent.thread.dao;

import com.concurrent.thread.entity.User;
import com.concurrent.thread.entity.Users;
import com.concurrent.thread.util.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ThinkPad
 */
//@Repository
public interface UserMapper extends BaseMapper<User> {
    void batchAdd(List<Users> user);
}
