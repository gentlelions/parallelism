package com.concurrent.thread.controller;

import com.concurrent.thread.service.UserService;
import com.concurrent.thread.util.lock.CacheLock;
import com.concurrent.thread.util.lock.CacheParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mr.yuan
 * @version v1.0
 * @date 2019/9/26 14:01
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;


    @CacheLock(prefix = "user")
    @RequestMapping("/batchadd")
    public void batchAdd(@CacheParam @RequestParam String token){
        try {
            service.batchAdd();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @CacheLock(prefix = "user")
    @RequestMapping("/add")
    public String add(@CacheParam @RequestParam String token){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("进来了");
        return "success - "+token;
    }
}
