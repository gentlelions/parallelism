package com.concurrent.thread.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.yuan
 * @version v1.0
 * @date 2019/9/26 13:59
 **/
@Data
@Builder
public class User implements Serializable {

    private int id;
    private String account;
    private String password;
    private Integer age;
    private String address;
    private Date birth;
    private String name;
    private String mobile;
}
