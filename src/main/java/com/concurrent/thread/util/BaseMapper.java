package com.concurrent.thread.util;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author Mr.yuan
 * @version v1.0
 * @date 2019/9/26 13:54
 **/
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
