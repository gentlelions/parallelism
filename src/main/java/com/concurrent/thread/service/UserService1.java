package com.concurrent.thread.service;

import com.concurrent.thread.dao.UserMapper;
import com.concurrent.thread.entity.User;
import org.springframework.stereotype.Component;

/**
 * @author Mr.yuan
 * @version v1.0
 * @date 2019/9/26 14:13
 **/
@Component
public class UserService1 {

    Integer call(User user, UserMapper userMapper){

        try {
            userMapper.insert(user);
            Integer.parseInt("aa");
            return 1;
        }catch (Exception e){
            System.out.println(Thread.currentThread().getName()+"failure");
        }
        return 0;
    }
}
